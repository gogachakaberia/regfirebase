package com.example.gogafirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPassword2: EditText
    private lateinit var RegisterButton: Button




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()
        registerListeners()

    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword2 = findViewById(R.id.editTextPassword2)
        RegisterButton = findViewById(R.id.RegisterButton)
    }

    private fun registerListeners() {
        RegisterButton.setOnClickListener {

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val password2 = editTextPassword2.text.toString()


            if (email.isEmpty() || password.isEmpty() || password2.isEmpty() || password.length <= 8 || password != password2 || !(email.contains('@')) ||
                    password.contains('1') && password.contains('2') && password.contains('3') && password.contains('4') && password.contains('5') && password.contains('6')
                        && password.contains('7') && password.contains('8') && password.contains('9') && password.contains('0')) {

                    Toast.makeText(this, "შეიყვანეთ პარამეტრები სწორად", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show()

                    }
                }




        }
    }



}