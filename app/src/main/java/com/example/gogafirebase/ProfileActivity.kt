package com.example.gogafirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class ProfileActivity : AppCompatActivity() {

    private lateinit var textView: TextView
    private lateinit var LogoutButton: Button




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
        registerListener()

        textView.text = FirebaseAuth.getInstance().currentUser?.uid

    }

    private fun init() {

        textView = findViewById(R.id.textView)
        LogoutButton = findViewById(R.id.LogoutButton)
    }

    private fun registerListener() {
        LogoutButton.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

}